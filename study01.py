# from pywinauto import Application
from pywinauto.timings import wait_until,Timings

# 连接Windows应用
# app=Application(backend="uia").connect(title_re="百度")
# 获取窗口
# 方式一
# win=app.window()
# 方式二
# win=app["百度一下，你就知道 - Google Chrome"]
# 最小应用程序
# win.minimize()
# 输出打印窗口中的所有控件
# win.print_control_identifiers()

# 窗口操作方法
'''
窗口最大化：maximize()
窗口最小化：minimize()
还原窗口正常大小：restore()
获取窗口显示状态：get_show_state()，最大化：1，正常：0
关闭窗口：close()
获取窗口坐标：rectangle() mid_point()获取中心点
'''
# ws = win.get_show_state()
# if ws == 0:
#     win.maximize()

# win.minimize() 注意：最小化之后就获取不到窗口目标了
# win.restore()
# win.close()
# rect=win.rectangle()

# 控件相关操作
'''
打印所有控件：print_control_identifiers()
'''
# win.print_control_identifiers()
# 选择控件
# 方式一
# edit1=win.Edit1
# 方式二，推荐
# edit1 = win["Edit1"]
# 方式三
# edit1=win.child_window(auto_id="kw", control_type="Edit")

'''
窗口控件的分类
状态栏：StatusBar 静态内容：Static 按钮：Button 复选框：CheckBox
单选框：RadioButton 组框：GroupBox 组合框：ComboBox 对话框：Dialog
编辑栏：Edit 头部内容：Header 列表框：ListBox 列表显示控件：ListView
弹出菜单：PopupMenu 选项卡控件：TabControl 工具栏：Toolbar 工具提示：ToolTips
树状视图：Tree View 
菜单：Menu 菜单项：MenuItem 窗格：Pane
'''

'''
控件相关属性获取
获取控件类型：wrapper_object()
获取该控件支持的方法；print(dir(a.wrapper_object()))
获取控件子元素：children
获取控件类名：class_name
以字典形式返回控件的属性：get_properties
'''
# wo=edit1.wrapper_object()
# print(dir(wo))
# print(edit1.children())
# print(edit1.class_name())
# print(edit1.get_properties())

'''
窗口及控件截图处理， 安装Pillow : pip install Pillow
截图处理：capture_as_image
截图保存：save("文件名")
'''
# pic = win.capture_as_image()
# print(pic)
# pic.save("baidu.png")

'''
菜单控件的相关操作
'''
# 获取菜单的子菜单项
# print(edit1.items())
# 通过下标选择菜单项
# menu.item_by_index(0)
# 通过路径选择菜单项
# menu.item_by_path("文件->新建连接...")

#菜单项的操作方法
#获取菜单项的所有子选项
# file.items()
#点击菜单项的方法
# file.click_input()

'''
等待机制
1、wait()  2、wait_not() 
wait_for: 等待的状态
    exists：表示该窗口是有效的句柄
    visible：表示该窗口未隐藏
    enabled：表示为禁用窗口
    ready：表示该窗口可见并启用
    active：表示该窗口处于活动状态
timeout：超时时间
retry_interval：重试时间间隔

3、wait_cpu_usage_lower()
    trreshold:该进程cpu占用率
    timeout：超时时间
    retry_interval: 重试时间间隔
'''
# dlg.wait(wait_for="ready",timeout=10,retry_interval=1)

'''
wait_until方法
    timeout：超时时间
    retry_interval:重试时间
    func：执行的函数
    value：比较的值
    Op：比较方式函数（默认为相等）
    args：给执行函数传位置参数
    kwargs：给执行函数传关键字参数
'''
# num = 0
# def get():
#     global num
#     num += 1
#     print("当前i的值为", num)
#     return num
# wait_until(10,1,get,5)

'''
全局等待时间控制
'''
# 将等待的计时器设为默认值
# Timings.defaults()
# 将等待时间加倍
# Timings.slow()
# 将等待时间减半
# Timings.fast()

'''
编辑类型的操作
type_keys:键入值
'''
# edit1.type_keys("日历")

'''
模拟用户键盘操作
ESC:VK_ESCAPE 回车：VK_RETURN
TAB键：VK_TAB 左WIN键：VK_LWIN
'''
from pywinauto.keyboard import send_keys

# 按F1
# send_keys("{F1}")
# send_keys("{VK_F1}")

# 组合按键
# send_keys("{VK_LWIN}cmd{VK_RETURN}")

'''
快捷键修饰符
+ -- shift
^ -- ctrl
% -- alt
^s -- ctrl+s
'''

'''
模拟鼠标操作   pywinauto.mouse
click: 鼠标单击
doubl_click: 鼠标双击
right_click: 鼠标右击
wheel_click: 鼠标中间点击
press：按下鼠标
repleace：释放鼠标
move：鼠标移动
scroll：滚动鼠标
'''
from pywinauto import mouse
#滚动鼠标
mouse.scroll(coords=(1000,500),wheel_dist=5)

